// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAoNC3LsmBM4hxBSvxCbAW9KmpHtiAs07g",
    authDomain: "dental-77ee0.firebaseapp.com",
    projectId: "dental-77ee0",
    storageBucket: "dental-77ee0.appspot.com",
    messagingSenderId: "1023040795332",
    appId: "1:1023040795332:web:ad4f70d119cdb637bd0f91",
    measurementId: "G-2G34JL39V0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
