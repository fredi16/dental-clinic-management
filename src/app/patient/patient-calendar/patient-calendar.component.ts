import { Component, 
  OnInit, 
  OnDestroy,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef, } from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { MatDialog } from '@angular/material/dialog';
import { PatientCalendarDialogComponent } from '../patient-calendar-dialog/patient-calendar-dialog.component';
import { AppointmentsService } from 'src/app/services/appointments.service';
import { takeUntil } from 'rxjs/operators';
import firebase from "firebase/app";
import { AuthenticationService } from 'src/app/services/authentication.service';

const colors: any = {
  purple: {
    primary: '#673ab7',
    secondary: '#3f51b5',
  },
  black: {
    primary: '#000000',
    secondary: '#808080',
  },
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'app-patient-calendar',
  templateUrl: './patient-calendar.component.html',
  styleUrls: ['./patient-calendar.component.css']
})
export class PatientCalendarComponent implements OnInit {

  @ViewChild('modalContent', { static: true }) modalContent!: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  ngUnsub = new Subject();

  refresh: Subject<any> = new Subject();

  events!: CalendarEvent[];

  activeDayIsOpen: boolean = true;

  constructor(public dialog: MatDialog, private appointmentsService: AppointmentsService, private authService: AuthenticationService) {}

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.dialog.open(PatientCalendarDialogComponent, {
      data: event
    });
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event) => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  ngOnInit(): void {
    this.appointmentsService.getAppointments()
    .pipe(takeUntil(this.ngUnsub))
    .subscribe(e => {
      this.events = []
      e.forEach(event => {
        if(event.title === this.authService.getPatientName()){
          this.events.push({
            id: event.id,
            start: event.start.toDate(),
            end: event.end.toDate(),
            title: event.title,
            color: colors.purple,
          });
        }else{
          this.events.push({
            id: event.id,
            start: event.start.toDate(),
            end: event.end.toDate(),
            title: 'Patient',
            color: colors.black,
          });
        }
      });
      this.refresh.next();
    })

  }
  ngOnDestroy(){
    this.ngUnsub.next();
    this.ngUnsub.complete();
  }


}
