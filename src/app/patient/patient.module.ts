import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientCalendarComponent } from './patient-calendar/patient-calendar.component';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarUtilsModule } from '../clinic/appointments/calendar-utils/calendar-utils.module';
import { MaterialModule } from 'src/app/material/material.module';
import { RouterModule } from '@angular/router';
import { PatientCalendarDialogComponent } from './patient-calendar-dialog/patient-calendar-dialog.component';

@NgModule({
  declarations: [
    PatientCalendarComponent,
    PatientCalendarDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CalendarUtilsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    MaterialModule
  ],
  exports: [
    PatientCalendarComponent,
    PatientCalendarDialogComponent
  ]
})
export class PatientModule { }
