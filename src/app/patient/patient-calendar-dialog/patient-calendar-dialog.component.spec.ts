import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientCalendarDialogComponent } from './patient-calendar-dialog.component';

describe('PatientCalendarDialogComponent', () => {
  let component: PatientCalendarDialogComponent;
  let fixture: ComponentFixture<PatientCalendarDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientCalendarDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientCalendarDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
