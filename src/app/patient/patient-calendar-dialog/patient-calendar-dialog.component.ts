import { Component, Inject, OnInit } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-calendar-dialog',
  templateUrl: './patient-calendar-dialog.component.html',
  styleUrls: ['./patient-calendar-dialog.component.css']
})
export class PatientCalendarDialogComponent implements OnInit {

  isEditVisable = this.data.title === JSON.parse(localStorage.getItem('user')!).patientName;

  constructor(@Inject(MAT_DIALOG_DATA) public data: CalendarEvent, private router: Router) {}

  ngOnInit(): void {
  }

  edit(){
    this.router.navigate([`pappointments/edit/${this.data.id}`])
  }

}
