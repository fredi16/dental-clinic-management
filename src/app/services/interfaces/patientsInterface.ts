import firebase from "firebase/app"; 
export interface patientsInterface {
    title: string,
    dob: firebase.firestore.Timestamp,
    phoneNumber: string
    gender: 'male' | 'female',
}