export interface userInfo {
    username: string,
    email: string,
    type: 'admin' | 'patient',
    patientName?: string
}