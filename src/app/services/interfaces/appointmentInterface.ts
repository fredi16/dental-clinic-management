import { CalendarEvent } from "angular-calendar";
import firebase from "firebase/app";
export interface appointmentInterface extends Omit<CalendarEvent, 'start' | 'end'> {
    description: string,
    start: firebase.firestore.Timestamp,
    end: firebase.firestore.Timestamp,
    startDate: firebase.firestore.Timestamp,
    startTime: string,
    endTime: string
}