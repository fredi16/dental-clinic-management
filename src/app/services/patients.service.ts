import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, first } from 'rxjs/operators';
import { appointmentInterface } from './interfaces/appointmentInterface';
import { patientsInterface } from './interfaces/patientsInterface';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  constructor(public afStore:AngularFirestore, private router: Router, private snackBar: MatSnackBar) { }

  createPatient(patientInfo: patientsInterface, patient: string){
    this.afStore.collection('/patients').doc(patient).set({
      title: patientInfo.title,
      dob: patientInfo.dob,
      phoneNumber: patientInfo.phoneNumber,
      gender: patientInfo.gender
    }).then(res => {
      this.snackBar.open("Patient created successfully", "Close");
      this.router.navigate(['patients']);
    }).catch((error) => {
      console.log(error);
    });
  }

  updatePatient(patientInfo: patientsInterface, patient: string){
    this.afStore.collection('/patients').doc(patient).update({
      title: patientInfo.title,
      dob: patientInfo.dob,
      phoneNumber: patientInfo.phoneNumber,
      gender: patientInfo.gender
    }).then(res => {
      this.snackBar.open("Patient updated successfully", "Close");
      this.router.navigate(['patients']);
    }).catch((error) => {
      console.log(error);
    });
  }

  getPatients(){
    return this.afStore.collection<patientsInterface>('/patients').valueChanges({ idField: 'id' }).pipe(first())
  }

  getPatientData(id: string){
    return this.afStore.collection<patientsInterface>('/patients').doc(id).valueChanges().pipe(first())
  }
  getPatientAppointments(title: string){
    return this.afStore.collection<appointmentInterface>('/appointments', ref => ref.where('title', '==', title)).valueChanges().pipe(first())
  }

  // deletePatients(id: string){
  //   this.afStore.collection('/patients').doc(id).delete()
  // }
}
