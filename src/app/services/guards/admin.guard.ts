import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate(){
    if(JSON.parse(localStorage.getItem('user')!).type === "admin"){
      return true;
    }else{
      alert("permission denied")
      this.router.navigate(["login"]);
      return false;
    }
  }
  
}
