import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthLoggedInGuard implements CanActivate {
  constructor(private router: Router){
  }
  canActivate() {
    if(!!localStorage.getItem('isLoggedIn')){
      let url: string = JSON.parse(localStorage.getItem('user')!).type === "admin" ? "appointments" : "pappointments";
      this.router.navigate([url]);
      return false;
    }else{
      return true;
    }
  }
  
}
