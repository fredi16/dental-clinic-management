import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PatientGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate(){
      if(JSON.parse(localStorage.getItem('user')!).type === "patient"){
        return true;
      }else{
        alert("permission denied")
        this.router.navigate(["login"]);
        return false;
      }
  }
  
}
