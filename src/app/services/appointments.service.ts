import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { appointmentInterface } from './interfaces/appointmentInterface';
import { patientsInterface } from './interfaces/patientsInterface';

@Injectable({
  providedIn: 'root'
})
export class AppointmentsService {

  

  constructor(public afStore:AngularFirestore, private router: Router, private snackBar: MatSnackBar) { }

  createAppointment(form: appointmentInterface){
    this.afStore.collection('/appointments').doc().set({
      title: form.title,
      description: form.description,
      start: form.start,
      end: form.end,
      startDate: form.startDate,
      startTime: form.startTime,
      endTime: form.endTime
    }).then(res => {
      this.snackBar.open("Appointment created successfully", "Close");
      let url: string = JSON.parse(localStorage.getItem('user')!).type === "admin" ? "appointments" : "pappointments";
      this.router.navigate([url]);
    }).catch((error) => {
      console.log(error);
    });
  }

  updateAppointment(form: appointmentInterface, id: string){
    this.afStore.collection('/appointments').doc(id).update({
      title: form.title,
      description: form.description,
      start: form.start,
      end: form.end,
      startDate: form.startDate,
      startTime: form.startTime,
      endTime: form.endTime
    }).then(res => {
      this.snackBar.open("Appointment updated successfully", "Close");
      let url: string = JSON.parse(localStorage.getItem('user')!).type === "admin" ? "appointments" : "pappointments";
      this.router.navigate([url]);
    }).catch((error) => {
      console.log(error);
    });
  }

  getAppointments(){
    return this.afStore.collection<appointmentInterface>('/appointments').valueChanges({ idField: 'id' })
  }

  getAppointment(id: string){
    return this.afStore.collection<appointmentInterface>('/appointments').doc(id).valueChanges().pipe(first())
  }

  deleteAppointment(id: string){
    this.afStore.collection<appointmentInterface>('/appointments').doc(id).delete();
  }

}
