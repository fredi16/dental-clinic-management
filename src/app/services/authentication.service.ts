import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { newUserForm } from './interfaces/newUserForm';
import { AngularFirestore } from '@angular/fire/firestore';
import { userInfo } from './interfaces/userInfo';
import { MatSnackBar } from '@angular/material/snack-bar';
import { first } from 'rxjs/operators';
import { patientsInterface } from './interfaces/patientsInterface';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  usersDb!: AngularFireList<any>;
  user!: AngularFireObject<any>;
  userInfo!: userInfo;
  // public currentUser!: Observable<firebaseUser.User>;

  constructor(public afStore:AngularFirestore, public db: AngularFireDatabase, public afAuth: AngularFireAuth, private router: Router, private snackBar: MatSnackBar) {
    // this.currentUser = this.afAuth.authState.pipe(filter((user: firebaseUser.User | null): user is firebaseUser.User => !!user));
    // this.currentUser.subscribe(x => this.uid = x.uid);
  }

  signUpAdmin(userData: newUserForm) {
    this.afAuth.createUserWithEmailAndPassword(userData.email, userData.password)
      .then((result: any) => {
        let id = result.user.uid;
        this.afStore.collection('/accounts').doc(id).set({
          username: userData.username,
          email: userData.email,
          type: userData.type
          }).catch(error => {
            console.log(error);
        })
        this.snackBar.open("Account created", "Close");
        this.router.navigate(["/login"]);
    }).catch(e => {
      this.snackBar.open("Email already in use", "Close");
    })
  }
  signUpPatient(userData: newUserForm, patientData: patientsInterface) {
    this.afAuth.createUserWithEmailAndPassword(userData.email, userData.password)
      .then((result: any) => {

        let patientName = this.generatePatientName(patientData.title, patientData.dob.toDate());

        let id = result.user.uid;
        this.afStore.collection('/accounts').doc(id).set({
          username: userData.username,
          email: userData.email,
          type: userData.type,
          patientName: patientName
          }).catch(error => {
            console.log(error);
        })
        
        this.afStore.collection('/patients').doc(patientName).get()
          .pipe(first())
          .subscribe(el => {
            if(!el.exists){
              this.afStore.collection('/patients').doc(patientName).set({
                title: patientData.title,
                dob: patientData.dob,
                phoneNumber: patientData.phoneNumber,
                gender: patientData.gender
              }).then(res => {
                this.snackBar.open("Account created", "Close");
                this.router.navigate(['login']);
              }).catch((error) => {
                console.log(error);
              })
              }
            }
          )

        this.snackBar.open("Account created", "Close");
        this.router.navigate(["/login"]);
    }).catch(e => {
      console.log(e);
      this.snackBar.open("Email already in use", "Close");
    })
  }

  signIn(loginForm: any){
      this.afAuth.signInWithEmailAndPassword(loginForm.email, loginForm.password).then(result=>{
      let id = result.user?.uid;
      this.afStore
        .collection<userInfo>('/accounts')
        .doc(id)
        .valueChanges({idField: 'propertyId'})
        .pipe(first())
        .subscribe((e) => {
          let info = {
            username: e?.username,
            email: e?.email,
            type: e?.type,
            patientName: e?.patientName
          };
          localStorage.setItem('user', JSON.stringify(info));
          localStorage.setItem('id', id as string)
          localStorage.setItem('isLoggedIn', 'true');
          this.router.navigate(['']);
          });
      }).catch(error => {
        this.snackBar.open("Invalid email or password", "Close");
      });
    }

    signOut(){
      this.afAuth.signOut().then( () =>{
        localStorage.clear();
        this.router.navigate(["login"]);
      });
    }

    isAuthenticated(){
      return !!localStorage.getItem('isLoggedIn');
    }

    isAdmin(){
      if(localStorage.getItem('user') === null) return false
      return JSON.parse(localStorage.getItem('user')!).type === "admin"
    }

    getPatientName(){
      return JSON.parse(localStorage.getItem('user')!).patientName
    }

    getUserInfo(){
      this.userInfo = JSON.parse(localStorage.getItem('user')!);
      return this.userInfo;
    }
    
    generatePatientName(name: string, date: Date): string{
      let newdate = new Date(date);
      return name.toUpperCase() + ' ' + '(' + (newdate.getDate() + '.' + (newdate.getMonth()+1) + '.' + newdate.getFullYear()) + ')';
    }
}



