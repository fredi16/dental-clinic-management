import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showFiller = false;
  username!: string;

  constructor(public router: Router, public service: AuthenticationService) {
  }

  ngOnInit() {
  }

  Logout() {
    this.service.signOut();
  };
}
