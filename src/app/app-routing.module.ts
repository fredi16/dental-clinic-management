import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { CalendarComponent } from './clinic/appointments/calendar/calendar.component';
import { CreateAppointmentComponent } from './clinic/appointments/create-appointment/create-appointment.component';
import { EditAppointmentComponent } from './clinic/appointments/edit-appointment/edit-appointment.component';
import { CreatePatientsComponent } from './clinic/patients/create-patients/create-patients.component';
import { PatientsDashboardComponent } from './clinic/patients/patients-dashboard/patients-dashboard.component';
import { UpdatePatientsComponent } from './clinic/patients/update-patients/update-patients.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PatientCalendarComponent } from './patient/patient-calendar/patient-calendar.component';
import { AdminGuard } from './services/guards/admin.guard';
import { AuthLoggedInGuard } from './services/guards/authLoggedIn.guard';
import { PatientGuard } from './services/guards/patient.guard';

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    redirectTo: ''
  },
  {path: "login", component: LoginComponent, canActivate: [AuthLoggedInGuard]},
  {path: "signup", component: SignupComponent, canActivate: [AuthLoggedInGuard]},
  {path: 'appointments', component: CalendarComponent, pathMatch: 'full', canActivate: [AdminGuard]},
  {path: 'appointments/create', component: CreateAppointmentComponent, canActivate: [AdminGuard]},
  {path: 'appointments/edit/:id', component: EditAppointmentComponent, canActivate: [AdminGuard]},
  {path: 'patients', component: PatientsDashboardComponent, pathMatch: 'full', canActivate: [AdminGuard]},
  {path: 'patients/create', component: CreatePatientsComponent, canActivate: [AdminGuard]},
  {path: 'patients/update/:id', component: UpdatePatientsComponent, canActivate: [AdminGuard]},
  {path: 'pappointments', component: PatientCalendarComponent, pathMatch: 'full', canActivate: [PatientGuard]},
  {path: 'pappointments/create', component: CreateAppointmentComponent , canActivate: [PatientGuard]},
  {path: 'pappointments/edit/:id', component: EditAppointmentComponent , canActivate: [PatientGuard]},
  {path: "**", component: PageNotFoundComponent}
  ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }