import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentsModule } from './appointments/appointments.module';
import { PatientsModule } from './patients/patients.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AppointmentsModule,
    PatientsModule
  ]
})
export class ClinicModule { }
