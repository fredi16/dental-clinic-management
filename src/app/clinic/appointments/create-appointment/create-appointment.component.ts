import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { first, map, startWith } from 'rxjs/operators';
import { AppointmentsService } from 'src/app/services/appointments.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PatientsService } from 'src/app/services/patients.service';

@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.css']
})
export class CreateAppointmentComponent implements OnInit {


  options: string[] = [];
  filteredOptions!: Observable<string[]>;

  newAppointment!: FormGroup;

  patientName!: string;


  constructor(private fb: FormBuilder, private appointmentsService: AppointmentsService, private patientsService: PatientsService, public authService: AuthenticationService) { 
    this.newAppointment = this.fb.group({
      title: ["", [Validators.required]],
      description: ["", [Validators.required, Validators.maxLength(500)]],
      startDate: ["", [Validators.required]],
      startTime: ["", [Validators.required]],
      endTime: ["", [Validators.required]]
    }, {
      // cross-field validation
    });

  }

  ngOnInit(): void {

    if(!!JSON.parse(localStorage.getItem('user')!).patientName){
      this.patientName = JSON.parse(localStorage.getItem('user')!).patientName
      this.newAppointment.setValue({
        ...this.newAppointment.value,
        title: this.patientName
      })
    }
    
    this.patientsService.getPatients().subscribe(e => {
      e.forEach(patient => {
        this.options.push(patient.id);
      })
    })

    this.filteredOptions = this.newAppointment.get('title')!.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  onSubmit(){
    let start = this.calculateDate(this.newAppointment.value.startDate, this.newAppointment.value.startTime);
    let end = this.calculateDate(this.newAppointment.value.startDate, this.newAppointment.value.endTime)
    if(!this.newAppointment.valid) return
    this.appointmentsService.createAppointment({...this.newAppointment.value, start, end})
    //this.newAppointment.reset();
  }

  calculateDate(date : Date, time: string): Date{
    let newdate = new Date(date);
    let hours = Number.parseInt(time[0]+time[1])
    let min = Number.parseInt(time[3]+time[4])
    newdate.setHours(hours, min)
    return newdate
  }

  get Title(){
    return this.newAppointment.value.title
  }

}
