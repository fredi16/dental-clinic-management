import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AppointmentsService } from 'src/app/services/appointments.service';
import { appointmentInterface } from 'src/app/services/interfaces/appointmentInterface';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  data!: appointmentInterface;

  constructor(@Inject(MAT_DIALOG_DATA) public id: string, private appointmentsService: AppointmentsService, private router: Router) {}

  ngOnInit(): void {

    this.appointmentsService.getAppointment(this.id).subscribe(e => {

      this.data = e!;

    })

  }

  delete(){
    this.appointmentsService.deleteAppointment(this.id as string);
  }
  edit(){
    this.router.navigate([`appointments/edit/${this.id}`])
  }

}
