import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarComponent } from './calendar/calendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarUtilsModule } from './calendar-utils/calendar-utils.module';
import { MaterialModule } from 'src/app/material/material.module';
import { DialogComponent } from './dialog/dialog.component';
import { RouterModule } from '@angular/router';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import { EditAppointmentComponent } from './edit-appointment/edit-appointment.component';

@NgModule({
  declarations: [
    CalendarComponent,
    DialogComponent,
    CreateAppointmentComponent,
    EditAppointmentComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CalendarUtilsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    MaterialModule
  ],
  exports: [
    CalendarComponent,
    DialogComponent,
    CreateAppointmentComponent,
    EditAppointmentComponent
  ]
})
export class AppointmentsModule { }
