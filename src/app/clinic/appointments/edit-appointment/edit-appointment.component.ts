import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map, startWith } from 'rxjs/operators';
import { AppointmentsService } from 'src/app/services/appointments.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PatientsService } from 'src/app/services/patients.service';

@Component({
  selector: 'app-edit-appointment',
  templateUrl: './edit-appointment.component.html',
  styleUrls: ['./edit-appointment.component.css']
})
export class EditAppointmentComponent implements OnInit {

  options: string[] = [];
  filteredOptions!: Observable<string[]>;

  appointmentId = this.route.snapshot.paramMap.get('id')!;

  newAppointment!: FormGroup;

  patientName = JSON.parse(localStorage.getItem('user')!).patientName;

  constructor(private fb: FormBuilder, private appointmentsService: AppointmentsService, private route: ActivatedRoute, private patientsService: PatientsService, public authService: AuthenticationService) { 
    this.newAppointment = this.fb.group({
      title: ["", [Validators.required]],
      description: ["", [Validators.required, Validators.maxLength(500)]],
      startDate: ["", [Validators.required]],
      startTime: ["", [Validators.required]],
      endTime: ["", [Validators.required]]
    }, {
      // cross-field validation
    });

  }

  ngOnInit(): void {

    this.appointmentsService.getAppointment(this.appointmentId).subscribe(e => {
      let startDate = e?.startDate.toDate();
      this.newAppointment.setValue({
        title: !!JSON.parse(localStorage.getItem('user')!).patientName ? JSON.parse(localStorage.getItem('user')!).patientName : e?.title,
        description: e?.description,
        startDate: startDate,
        startTime: e?.startTime,
        endTime: e?.endTime
      })
    })
    
    this.patientsService.getPatients().subscribe(e => {
      e.forEach(patient => {
        this.options.push(patient.id);
      })
    })

    this.filteredOptions = this.newAppointment.get('title')!.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  onSubmit(){
    let start = this.calculateDate(this.newAppointment.value.startDate, this.newAppointment.value.startTime);
    let end = this.calculateDate(this.newAppointment.value.startDate, this.newAppointment.value.endTime);
    if(!this.newAppointment.valid) return
    this.appointmentsService.updateAppointment({...this.newAppointment.value, start, end}, this.appointmentId);
    //this.newAppointment.reset();
  }

  calculateDate(date : Date, time: string): Date{
    let newdate = new Date(date);
    let hours = Number.parseInt(time[0]+time[1])
    let min = Number.parseInt(time[3]+time[4])
    newdate.setHours(hours, min)
    return newdate
  }

  get Title(){
    return this.newAppointment.value.title
  }

}
