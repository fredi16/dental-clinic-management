import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PatientsService } from 'src/app/services/patients.service';

@Component({
  selector: 'app-update-patients',
  templateUrl: './update-patients.component.html',
  styleUrls: ['./update-patients.component.css']
})
export class UpdatePatientsComponent implements OnInit {

  newPatient!: FormGroup;

  patientId = this.route.snapshot.paramMap.get('id')!;

  constructor(private fb: FormBuilder, private patientsService: PatientsService, private route: ActivatedRoute) { 
    this.newPatient = this.fb.group({
      title: ["", [Validators.required]],
      dob: ["", [Validators.required]],
      phoneNumber: ["", [Validators.required]],
      gender: ["", [Validators.required]]
    }, {
      // cross-field validation
    });

  }

  ngOnInit(): void {
    console.log(this.patientId);

    this.patientsService.getPatientData(this.patientId).subscribe(e => {
      this.newPatient.setValue({
        title: e?.title,
        dob: e?.dob.toDate(),
        phoneNumber: e?.phoneNumber,
        gender: e?.gender
      })
    })

  }

  onSubmit(){
    let patient = this.patientName(this.newPatient.value.title, this.newPatient.value.dob);
    if(!this.newPatient.valid) return
    this.patientsService.updatePatient({...this.newPatient.value}, patient)
    //this.newAppointment.reset();
  }

  patientName(name: string, date : Date): string{
    let newdate = new Date(date);
    return name.toUpperCase() + ' ' + '(' + (newdate.getDate() + '.' + (newdate.getMonth()+1) + '.' + newdate.getFullYear()) + ')';
  }

  get Title(){
    return this.newPatient.value.title
  }

}
