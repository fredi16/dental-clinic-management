import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { appointmentInterface } from 'src/app/services/interfaces/appointmentInterface';
import { patientsInterface } from 'src/app/services/interfaces/patientsInterface';
import { PatientsService } from 'src/app/services/patients.service';

@Component({
  selector: 'app-patient-dialog',
  templateUrl: './patient-dialog.component.html',
  styleUrls: ['./patient-dialog.component.css']
})
export class PatientDialogComponent implements OnInit {

  appointments: appointmentInterface[] = [];
  patient!: patientsInterface;

  constructor(@Inject(MAT_DIALOG_DATA) public id: string, private patientsService: PatientsService, private router: Router) {}

  ngOnInit(): void {

    this.patientsService.getPatientData(this.id).subscribe(e => {
      this.patient = e!;
      }
    );
    this.patientsService.getPatientAppointments(this.id).subscribe(e => {
      this.appointments = e;
      }
    );
  }

}
