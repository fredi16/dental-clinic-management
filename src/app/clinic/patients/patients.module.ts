import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientsDashboardComponent } from './patients-dashboard/patients-dashboard.component';
import { MaterialModule } from 'src/app/material/material.module';
import { RouterModule } from '@angular/router';
import { CreatePatientsComponent } from './create-patients/create-patients.component';
import { UpdatePatientsComponent } from './update-patients/update-patients.component';
import { FormsModule, ReactiveFormsModule,  } from '@angular/forms';
import { PatientDialogComponent } from './patient-dialog/patient-dialog.component';

@NgModule({
  declarations: [
    PatientsDashboardComponent,
    CreatePatientsComponent,
    UpdatePatientsComponent,
    PatientDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    PatientsDashboardComponent,
    CreatePatientsComponent,
    UpdatePatientsComponent
  ]
})
export class PatientsModule { }
