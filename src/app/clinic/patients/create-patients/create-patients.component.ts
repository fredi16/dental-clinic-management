import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PatientsService } from 'src/app/services/patients.service';

@Component({
  selector: 'app-create-patients',
  templateUrl: './create-patients.component.html',
  styleUrls: ['./create-patients.component.css']
})
export class CreatePatientsComponent implements OnInit {

  newPatient!: FormGroup;

  constructor(private fb: FormBuilder, private patientsService: PatientsService) { 
    this.newPatient = this.fb.group({
      title: ["", [Validators.required]],
      dob: ["", [Validators.required]],
      phoneNumber: ["", [Validators.required]],
      gender: ["", [Validators.required]]
    }, {
      // cross-field validation
    });

  }

  ngOnInit(): void {
  }

  onSubmit(){
    let patient = this.getPatientName(this.newPatient.value.title, this.newPatient.value.dob);
    if(!this.newPatient.valid) return
    this.patientsService.createPatient({...this.newPatient.value}, patient)
    //this.newAppointment.reset();
  }

  getPatientName(name: string, date : Date): string{
    let newdate = new Date(date);
    return name.toUpperCase() + ' ' + '(' + (newdate.getDate() + '.' + (newdate.getMonth()+1) + '.' + newdate.getFullYear()) + ')';
  }

  get Title(){
    return this.newPatient.value.title
  }

}
