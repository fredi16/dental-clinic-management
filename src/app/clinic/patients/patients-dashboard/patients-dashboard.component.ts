import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PatientsService } from 'src/app/services/patients.service';
import { MatDialog } from '@angular/material/dialog';
import { PatientDialogComponent } from '../patient-dialog/patient-dialog.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export interface Patients {
  title: string;
  dob: Date;
  propertyId: string;
}

let ELEMENT_DATA: Patients[] = [];

@Component({
  selector: 'app-patients-dashboard',
  templateUrl: './patients-dashboard.component.html',
  styleUrls: ['./patients-dashboard.component.css']
})
export class PatientsDashboardComponent implements OnInit {

  displayedColumns: string[] = ['title', 'dob', 'actions'];

  isLoadingResults = true;

  ngUnsub = new Subject();

  constructor(public dialog: MatDialog, private patientsService: PatientsService, public router: Router) { 
  }

  ngOnInit(): void {
    this.loadData();
  }
  
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  loadData(){
    this.patientsService.getPatients().pipe(takeUntil(this.ngUnsub))
    .subscribe(e => {
      ELEMENT_DATA = [];
      e.forEach(data => {
        ELEMENT_DATA.push({title: data.title, dob: data.dob.toDate(), propertyId: data.id})
      });
      this.isLoadingResults = false;
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    });
  }

  // onDelete(i: number){
  //   this.patientsService.deletePatients(ELEMENT_DATA[i].propertyId);
  //   this.isLoadingResults = true;
  //   this.loadData();
  // }

  onView(i: number){
    this.dialog.open(PatientDialogComponent, {
      data: ELEMENT_DATA[i].propertyId,
    });
  }
  onEdit(i: number){
    this.router.navigate(['patients/update/', ELEMENT_DATA[i].propertyId]);
  }

  ngOnDestroy(){
    this.ngUnsub.next();
    this.ngUnsub.complete();
  }

}
