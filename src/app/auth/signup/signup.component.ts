import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { passwordValidator } from './validators/password.validator';
import { passwordMatchValidator } from './validators/passwordMatch.validator';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import firebase from "firebase/app";

class CrossFieldErrorMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, signUpForm: FormGroupDirective | NgForm | null): boolean {
    return !!control?.dirty && (control.value !== signUpForm?.value.password);
  }
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  errorMatcher = new CrossFieldErrorMatcher();

  signUpForm!: FormGroup;
  secondFormGroup!: FormGroup;

  constructor(private fb: FormBuilder, private authService:AuthenticationService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    // form validation
    this.signUpForm = this.fb.group({
      username: ["", [Validators.required]],
      email: ["", [Validators.required]],
      password: ["", [Validators.required, passwordValidator(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)]],
      cpassword: ["", [Validators.required]],
      type: ["", [Validators.required]]
    }, {
      // cross-field validation
      validator: passwordMatchValidator
    });

    this.secondFormGroup = this.fb.group({
      title: ['', Validators.required],
      dob: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      gender: [''],
    });
  }

  passwordValidator(form: FormGroup) {
    const condition = form.get('password')?.value !== form.get('cpassword')?.value;

    return condition ? { passwordsDoNotMatch: true} : null;
  }

  onSubmit(){
    if(!this.signUpForm.valid) return
    if(this.signUpForm.value.type === 'admin'){
      this.authService.signUpAdmin(this.signUpForm.value);
    }
    if(this.signUpForm.value.type === 'patient'){
      this.secondFormGroup.setValue({
        ...this.secondFormGroup.value,
        dob: firebase.firestore.Timestamp.fromDate(this.secondFormGroup.value.dob)
      });
      this.authService.signUpPatient(this.signUpForm.value, this.secondFormGroup.value);
    }
  }

  get userName(){
    return this.signUpForm.get('username')
  }
  get email(){
    return this.signUpForm.get('email')
  }
  get password(){
    return this.signUpForm.get('password')
  }
  get cpassword(){
    return this.signUpForm.get('cpassword')
  }

}
