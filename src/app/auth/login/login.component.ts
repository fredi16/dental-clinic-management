import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;

  error: any;
  
  form = {
    email: "",
    password: ""
  }


  constructor(private authService:AuthenticationService) {
  }
  
  ngOnInit(): void {
    
  }


  onSubmit(loginForm: NgForm){
    this.authService.signIn(loginForm.value);
    // loginForm.resetForm();
  }
}
